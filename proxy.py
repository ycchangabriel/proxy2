import asyncio
import serial_asyncio
import struct
import time

from datastructures import IoctlPacket, SysCommand, DataPacket
from exceptions import PeerDisconnectedError

print('proxy starts...')

try:
    with open('/boot/icenetcproxy.serialpath') as f:
        serial_path = f.readline().rstrip()
except FileNotFoundError:
    serial_path = None

SOCKET_PATH = '/tmp/iceqube_icenetc_proxy.sock'
SERIAL_PATH = serial_path or '/dev/cc1310_dongle'

print('SERIAL_PATH is set to', SERIAL_PATH)

class SocketProtocol(asyncio.Protocol):
    def __init__(self, loop, controller):
        self.loop = loop
        self.controller = controller
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        self.controller.on_socket_connection_made(transport)
        print('socket connected')

    def connection_lost(self, exc):
        print('socket disconnected')

    def data_received(self, data):
        print('   ||<<< socket data received', data)
        try:
            # assuming this method always receive EXACT number of bytes from user space
            ioctl = IoctlPacket.from_bytes(data)
            # when printing out it looks like this: 1074024990 b'\xcb\xd8z\xc2'
            coro = self.controller.on_ioctl_packet_received(ioctl)
            asyncio.ensure_future(coro)
        except struct.error or ValueError:
            # we assume everything not fit in an IoctlPacket are session data.
            # these data are not in packet form.
            icenetc_coro = self.controller.on_icenetc_command_received(data)
            asyncio.ensure_future(icenetc_coro)

    def notify_no_peer(self):
        """if peer session dropped unexpected, use this to notify user space the peer got disconnected
        """
        packet = struct.pack('=LL', 0xffffffff, 0x00000000)
        print('   ||>>> peer dropped', packet)
        self.transport.write(packet)

class SerialProtocol(asyncio.Protocol):
    def __init__(self, loop, controller):
        self.loop = loop
        self.controller = controller
        self.transport = None
        self.future = None
        self.icenetc_future = None

        self.state = self.HEADER
        self.buf = bytearray()

    (HEADER, PROTH, PROTL, LENH, LENL, DATA, FOOTER) = range(7)

    def process_one_byte(self, byte):
        """Process a byte.  If this byte completes a
        packet, return the packet otherwise return None.
        """
        if self.state == self.HEADER:
            if byte == b'<':
                self.buf.extend(byte)
                self.state += 1
            else:
                # Lost sync.  Reset.
                print(">> lost sync. reset.")
                self.buf = bytearray()
            return None
        elif self.state in [ self.PROTH, self.PROTL, self.LENH ]:
            self.buf.extend(byte)
            self.state += 1
            return None
        elif self.state == self.LENL:
            self.buf.extend(byte)
            self.expected_len = struct.unpack("<H", self.buf[-2:])[0]
            if self.expected_len < 800:
                self.state += 1
            else:
                self.state = self.HEADER
            return None
        elif self.state == self.DATA:
            self.buf.extend(byte)
            self.expected_len -= 1
            if self.expected_len == 0:
                self.state += 1
            return None
        elif self.state == self.FOOTER:
            self.buf.extend(byte)
            if byte == b'>':
                print('>>>||    serial received', bytes(self.buf))
                packet = DataPacket.from_bytes(self.buf)
                self.buf = bytearray()
                self.state = self.HEADER
                return packet
            else:
                self.state = self.HEADER
                return None
        else:
            assert 0, "Unreachable."

    def connection_made(self, transport):
        self.transport = transport
        print('serial connected')

    def connection_lost(self, exc):
        print('serial disconnected')

    def data_received(self, data):
        buf = struct.unpack('<' + str(len(data)) + 'c', data)
        for byte in buf:
            packet = self.process_one_byte(byte)

        if packet == None:
            return # wait for more data

        print('   ||    ^^ class representation', packet)

        if packet.protocol not in [0x0000, 0x0001, 0x8001]:
            raise NotImplementedError('unknown data packet protocol', packet)

        if packet.protocol == 0x0001:
            # these are icenetc response
            self.icenetc_future.set_result(packet)
            return

        syscmd = SysCommand.from_bytes(packet.data)
        if syscmd.cmd in [0x70, 0x71]:
            self.controller.on_session_control_packet(syscmd)
        elif packet.protocol == 0x8001:
            # retry logic.
            delay_ms = int.from_bytes(syscmd.data, byteorder='little') # double check is it little endian. matters for retry
            if delay_ms == 0x00000000:
                # good received. expect data come from serial
                return
            elif delay_ms == 0xffffffff:
                # radio tells proxy no active peer session
                self.icenetc_future.set_exception(PeerDisconnectedError)
            else:
                # the payload is how long wait until retry, in milliseconds
                print(f'         [INFO] retry in {delay_ms} ms')
                time.sleep(delay_ms/1000)
                raise NotImplementedError('never seen retry during development. try consistently reproduce and implement this retry feature :)')

        elif packet.protocol == 0x0000:
            # assume the remaining are sys command response
            # TODO: make a conversion of sys command from data class. do sanity check inside
            self.future.set_result(packet)
        else:
            raise NotImplementedError('unhandled case')

    def relay_ioctl(self, ioctl):
        self.future = self.loop.create_future()
        syscmd = SysCommand.from_ioctl(ioctl)
        data_packet = DataPacket.from_syscmd(syscmd)
        print('<<<||    writing to serial', bytes(data_packet))
        self.transport.write(bytes(data_packet))
        return self.future

    def relay_icenetc_command(self, cmd):
        self.icenetc_future = self.loop.create_future()
        data_packet = DataPacket(protocol=0x01, length=len(cmd), data=cmd)
        print('<<<||    writing icenetc to serial', bytes(data_packet))
        self.transport.write(bytes(data_packet))
        return self.icenetc_future

class Controller:
    def __init__(self, loop):
        self.loop = loop
        self.serial_transport = None
        self.socket_transport = None

        self.peer = 0

    async def startup(self):
        coro_serial = serial_asyncio.create_serial_connection(self.loop, lambda: SerialProtocol(loop, self), SERIAL_PATH, baudrate=115200)
        _, self.serial_transport = await asyncio.ensure_future(coro_serial)
        server = await self.loop.create_unix_server(lambda: SocketProtocol(loop, self), SOCKET_PATH)

    def on_socket_connection_made(self, transport):
        self.socket_transport = transport

    async def on_ioctl_packet_received(self, ioctl_packet):
        # depends on ioctl direction, we decide how many bytes to wait from serial
        # because before sending back to socket, the first four bytes indicate how many bytes following.
        if ioctl_packet.cmd_dir & 0x2:
            waiting_for = ioctl_packet.cmd_size
        else:
            waiting_for = 0

        serial_resp_packet:DataPacket = await self.serial_transport.relay_ioctl(ioctl_packet)
        data = serial_resp_packet.data

        if len(data) >= waiting_for:
            socket_resp = struct.pack(f'=L{waiting_for}s', waiting_for, data[1:waiting_for+1])
        else:
            socket_resp = struct.pack(f'=L', 0x00)

        print('   ||>>> writing to socket', socket_resp)
        print('--------')
        self.socket_transport.write(socket_resp)

    async def on_icenetc_command_received(self, icenetc_command):
        try:
            if self.peer == 0:
                raise PeerDisconnectedError

            resp:DataPacket = await self.serial_transport.relay_icenetc_command(icenetc_command)

            print('   ||>>> writing icenetc to socket', resp.data)
            self.socket_transport.write(resp.data)
        except PeerDisconnectedError:
            self.socket_transport.get_protocol().notify_no_peer()

    def on_session_control_packet(self, packet:SysCommand):
        if packet.cmd == 0x70:
            self.peer = packet.data
            print('         [INFO] peer connected:', self.peer)
            print('   ||>>> notifying socket peer connected', packet.data)
            self.socket_transport.write(packet.data)
        elif packet.cmd == 0x71:
            self.peer = 0
            print('         [INFO] peer disconnected')

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    controller = Controller(loop)
    loop.run_until_complete(controller.startup())

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print('received keyboard interrupt. terminating event loop')
        exit(0)
