def _ioc_dir(cmd):
    return (cmd >> 30) & 0x3

def _ioc_type(cmd):
    return (cmd >> 8) & 0xff

def _ioc_nr(cmd):
    return cmd & 0xff

def _ioc_size(cmd):
    return (cmd >> 16) & 0x3fff