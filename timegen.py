import time
from datetime import datetime
while True:
    with open("/tmp/timestamp.txt", "a") as f:
        f.write("The current timestamp is: " + str(datetime.now()) + "\n")
        f.close()
    time.sleep(1)
