import struct
from dataclasses import dataclass

from ioctlhelpers import _ioc_dir, _ioc_type, _ioc_size, _ioc_nr

@dataclass
class IoctlPacket:
    cmd: int
    data: bytes

    def __init__(self, cmd, data):
        if _ioc_type(cmd) != ord('R'):
            raise ValueError('invalid magic. expect R')
        if _ioc_size(cmd) > 4:
            raise ValueError('invalid ioctl size')
        if _ioc_nr(cmd) % 10 > 0:
            raise ValueError('invalid sys command. all sys command are divisible by 10')
        self.cmd = cmd
        self.data = data

    @property
    def cmd_dir(self):
        return _ioc_dir(self.cmd)
    @property
    def cmd_type(self):
        return _ioc_type(self.cmd)
    @property
    def cmd_nr(self):
        return _ioc_nr(self.cmd)
    @property
    def cmd_size(self):
        return _ioc_size(self.cmd)

    @staticmethod
    def from_bytes(b):
        cmd, data = struct.unpack('=L4s', b)
        return IoctlPacket(cmd, data)

    def __bytes__(self):
        return struct.pack('=L4s', self.cmd, self.data)

@dataclass
class SysCommand:
    cmd: int
    data: bytes

    @staticmethod
    def from_bytes(b):
        cmd, data = struct.unpack('=B4s', b)
        return SysCommand(cmd, data)

    @staticmethod
    def from_ioctl(ioctl_packet):
        ioc2syscmd = {
            10: 0x00, # SYSCMD_PING
            20: 0x01, # SYSCMD_GET_MAC
            30: 0x02, # SYSCMD_SET_MAC
            40: 0x03, # SYSCMD_GET_PROT
            50: 0x04, # SYSCMD_SET_PROT
            60: 0x05, # SYSCMD_GET_LISTEN_CHANS
            70: 0x06, # SYSCMD_SET_LISTEN_CHANS
            80: 0x07, # SYSCMD_GET_SESSION_CHANS
            90: 0x08, # SYSCMD_SET_SESSION_CHANS
            100: 0x09, # SYSCMD_CLOSE_SESSION
            110: 0x10, # SYSCMD_GET_VERSION
            130: 0x11, # SYSCMD_GET_REGULATORY
            140: 0x12, # SYSCMD_SET_REGULATORY
            150: 0x13, # SYSCMD_GET_LAST_RSSI
            160: 0x0B, # SYSCMD_SET_RANK
            170: 0x0A, # SYSCMD_GET_RANK
        }
        cmd = ioc2syscmd[ioctl_packet.cmd_nr]
        return SysCommand(cmd, ioctl_packet.data)

    def __bytes__(self):
        return struct.pack('=B4s', self.cmd, self.data)

@dataclass
class DataPacket:
    protocol: int
    length: int
    data: bytes

    @staticmethod
    def from_bytes(b):
        protocol, length = struct.unpack('<HH', b[1:5])
        data = b[5:-1]
        return DataPacket(protocol, length, data)

    @staticmethod
    def from_syscmd(syscmd):
        return DataPacket(protocol=0x00, length=5, data=bytes(syscmd))

    def __bytes__(self):
        return b'<' + struct.pack(f"<HH{self.length}s", self.protocol,
                self.length, self.data) + b'>'
